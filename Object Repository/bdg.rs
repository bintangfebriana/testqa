<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>bdg</name>
   <tag></tag>
   <elementGuidId>48637ff0-0019-40dc-89f9-727b8b099b72</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Bandung-Bandung-BD' or . = 'Bandung-Bandung-BD')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#content > div.css-14u6y52 > div.css-8atqhb.e1iwxiko0 > div.css-17uduf3 > div > div > div.css-1m9l7hb > section > div.css-1q62ntx > div > div:nth-child(2)</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Bandung-Bandung-BD</value>
      <webElementGuid>f0b95a76-56c6-4b1f-a7c0-eb793091a40c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>css</name>
      <type>Main</type>
      <value>css-128hibx</value>
      <webElementGuid>fc1f1d31-5ea0-46b6-ab5e-162e56f4e437</webElementGuid>
   </webElementProperties>
</WebElementEntity>
